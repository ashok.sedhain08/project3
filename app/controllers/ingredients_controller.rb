class IngredientsController < ApplicationController
  before_action :find_ingredient, only: %i[destroy]
  def show
   @ingredients = Ingredient.all  
  end

  def new
    @ingredient = Ingredient.new
  end

  def create
    @ingredient = Ingredient.new ingredient_params
    if @ingredient.save
      redirect_to ingredients_path
    else
      redirect_to ingredients_path
    end
  end

  def destroy
    if @ingredient.destroy 
      redirect_to ingredients_path
    else
      render :show
    end
  end

  private

  def ingredient_params
    params.require(:ingredient).permit(:name)
  end

  def find_ingredient
    @ingredient=Ingredient.find permitted_params["id"]
  end

  def permitted_params
    params.permit(:id)
  end
end
