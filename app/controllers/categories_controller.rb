class CategoriesController < ApplicationController
  before_action :find_category, only: %i[show edit update destroy]
  def index
    @categories = Category.all
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new category_params
    if @category.save
      redirect_to categories_path
    else
      flash[:error]=@book.error.full_messages.join(", ")
      render :new
    end
  end

  def edit
  end

  def update
    if @category.update category_params 
      redirect_to categories_path
    else
      render :edit
    end
  end
  def show
  end

  def destroy
    if @category.destroy 
      redirect_to categories_path
    else
      render :show
    end
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end

  def permitted_params
    params.permit(:id)
  end

  def find_category
    @category=Category.find permitted_params["id"]
  end
end